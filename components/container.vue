<script lang="ts">
import Vue from 'vue';

export default Vue.extend({
  name: 'SlpContainer',
  props: {
    fluid: {
      type: Boolean,
      required: false,
      default: false,
    },
    variant: {
      type: String,
      required: false,
      default: 'white',
    },
  },
  computed: {
    className(): string {
      return this.fluid ? 'fluid' : '';
    },
  },
});
</script>

<template>
  <div
    v-if="variant === 'light-purple-100'"
    class="slp-full-light-purple-background"
  >
    <div class="slp-container" :class="className">
      <slot></slot>
    </div>
  </div>
  <div v-else-if="variant === 'grey-100'" class="slp-charcoal-grey-background">
    <div class="slp-container" :class="className">
      <slot></slot>
    </div>
  </div>
  <div v-else-if="variant === 'gradient'" class="slp-gradient-background">
    <div class="slp-container" :class="className">
      <slot></slot>
    </div>
  </div>
  <div
    v-else-if="variant === 'charcoal-100'"
    class="slp-full-charcoal-background"
  >
    <div class="slp-container" :class="className">
      <slot></slot>
    </div>
  </div>
  <div v-else-if="variant === 'grey-50'" class="slp-half-grey-background">
    <div class="slp-container" :class="className">
      <slot></slot>
    </div>
  </div>
  <div v-else-if="variant === 'white-75'" class="slp-partial-white-background">
    <div class="slp-container" :class="className">
      <slot></slot>
    </div>
  </div>
  <div v-else-if="variant === 'grey-75'" class="slp-partial-grey-background">
    <div class="slp-container" :class="className">
      <slot></slot>
    </div>
  </div>
  <div v-else class="slp-container" :class="className">
    <slot></slot>
  </div>
</template>

<style lang="scss" scoped>
.#{$prefix}-container {
  width: 100%;
  margin-left: auto;
  margin-right: auto;
  padding-left: $spacing-16;
  padding-right: $spacing-16;
  border: 1px solid transparent;
}

@media (min-width: 640px) {
  .#{$prefix}-container {
    max-width: 95%;
  }
}

@media (min-width: $breakpoint-lg) {
  .#{$prefix}-container {
    max-width: $breakpoint-lg;
    padding: 0px;
  }
}

@media (min-width: 1170px) {
  .#{$prefix}-container {
    max-width: 1170px;
    padding: 0px;
  }
}

.slp-full-grey-background {
  background: $color-surface-100;
}

.slp-full-charcoal-background {
  background: $color-primary-200;
}

.slp-gradient-background {
  background: radial-gradient(
      50.08% 44.51% at 100% 100%,
      rgba(169, 137, 245, 0.2) 0%,
      rgba(119, 89, 194, 0) 100%
    ),
    radial-gradient(
      60.76% 48.01% at 0% 100%,
      rgba(226, 67, 41, 0.2) 0%,
      rgba(226, 67, 41, 0) 100%
    ),
    #ffffff;
}

.slp-full-light-purple-background {
  background: $color-surface-800;
}

.slp-half-grey-background {
  background-image: linear-gradient(
    to bottom,
    $color-surface-50 0% 50%,
    $color-surface-100 50% 100%
  );
}

.slp-partial-white-background {
  background-image: linear-gradient(
    to bottom,
    $color-surface-100 0% 70%,
    $color-surface-50 70% 100%
  );
}

.slp-partial-grey-background {
  background-image: linear-gradient(
    to bottom,
    $color-surface-50 0% 60%,
    $color-surface-100 60% 100%
  );
}

@media (max-width: $breakpoint-sm) {
  .slp-partial-grey-background {
    background-image: linear-gradient(
      to bottom,
      $color-surface-50 0% 30%,
      $color-surface-100 30% 100%
    );
  }
}

.fluid {
  max-width: 100%;
  margin: 0px;
  border: none;
  padding: 0px;
}
</style>
