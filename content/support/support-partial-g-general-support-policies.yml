---
  components:
    - name: copy
      data:
        block:
          - hide_horizontal_rule: true
            no_margin_bottom: true
            text: |
              ## GitLab.com Specific Support Policies {#gitlabcom-specific-support-policies}

              ### Account Recovery and 2FA Resets {#account-recovery-and-2fa-resets}

              If you are unable to sign into your account we can help you regain access. **This service is only available for paid users** (you are part of paid group or paid user namespace).

              **Forgotten password?**

              1. Use the [Reset password form](https://gitlab.com/users/password/new){data-ga-name="reset password" data-ga-location="body"}
              1. If you don't receive the reset email please [contact support](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000803379){data-ga-name="contact support" data-ga-location="support policies"}

              **Locked out by 2FA?**

              Are you on a free plan? [Please read this blog post](/blog/2020/08/04/gitlab-support-no-longer-processing-mfa-resets-for-free-users/){data-ga-name="mfa resets" data-ga-location="body"}

              1. Try to [generate new recovery keys using SSH](https://docs.gitlab.com/ee/user/profile/account/two_factor_authentication.html#generate-new-recovery-codes-using-ssh){data-ga-name="ssh recovery codes" data-ga-location="body"}
              1. Paid users only: If you are still unable to sign in after trying step one, [contact support](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000803379){data-ga-name="contact support" data-ga-location="support policies 2fa"}. We will request evidence to prove account ownership. We cannot guarantee to recover your account unless you pass the verification checks.

              Please note that in some cases reclaiming an account may be impossible. Read ["How to keep your GitLab account safe"](/blog/2018/08/09/keeping-your-account-safe/){data-ga-name="How to keep account safe" data-ga-location="body"} for advice on preventing this.

              ### Restoration of Deleted Data {#restoration-of-deleted-data}

              Any type of data restoration is currently a manual and time consuming process lead by GitLab’s infrastructure team. [Our infrastructure team clearly states](/handbook/engineering/infrastructure/faq/#q-if-a-customer-project-is-deleted-can-it-be-restored){data-ga-name="deleted project" data-ga-location="body"} that “once a project is deleted it cannot be restored”.

              We encourage customers to use:

              1. the [delayed deletion](https://docs.gitlab.com/ee/user/group/#enabling-delayed-project-removal){data-ga-name="project delayed deletion" data-ga-location="body"} feature if it is available in your plan,
              1. [export projects and/or groups](https://docs.gitlab.com/ee/user/project/settings/import_export.html){data-ga-name="import/export projects" data-ga-location="body"} regularly, particularly via the API.

              GitLab will consider restoration requests only when the request is for a project or group that is part of a **paid plan** with an active subscription applied, and one of the following is true:

              - The data was deleted due to a GitLab bug.
              - The organization’s contract includes a specific provision.

              Please note that user accounts and individual contributions cannot be restored.

              ### Ownership Disputes {#ownership-disputes}

              GitLab will not act as an arbitrator of Group or Account ownership disputes. Each user and group owner is responsible for ensuring that they are following best practices for data security.

              As GitLab subscriptions are generally business-to-business transactions, in the event that a former employee has revoked company access to a paid group, please contact GitLab Support for recovery options.

              ### Name Squatting Policy {#name-squatting-policy}

              Per the [GitLab Terms of Service](/terms/){data-ga-name="terms" data-ga-location="body"}:

              > Account name squatting is prohibited by GitLab. Account names on GitLab are administered to users on a first-come, first-serve basis. Accordingly, account names cannot be held or remain inactive for future use.

              The GitLab.com Support Team will consider a [namespace](https://docs.gitlab.com/ee/user/group/#namespaces){data-ga-name="namespace" data-ga-location="body"} (user name or group name) to fall under the provisions of this policy when the user has not logged in or otherwise used the namespace for an extended time.

              Namespaces will be released, if eligible under the criteria below, upon request by a member of a paid namespace or sales approved prospect.

              Specifically:

              - User namespaces can be reassigned if **both** of the following are true:

                 1. The user's last sign in was at least two years ago.
                 2. The user is not the sole owner of any active projects.

              - Group namespaces can be reassigned if **one** of the following is true:

                 1. There is no data (no project or project(s) are empty).
                 2. The owner's last sign in was at least two years ago.

              If the namespace contains data, GitLab Support will attempt to contact the owner over a two week period before reassigning the namespaces. If the namespace contains no data (empty or no projects) and the owner is inactive, the namespace will be released immediately.

              Namespaces associated with unconfirmed accounts over 90 days old are eligible for immediate release. Group namespaces that contain no data and were created more than 6 months ago are likewise eligible for immediate release.

              NOTE: The minimum characters required for a namespace is `2`, it is no longer possible to have a namespace of `1` character.

              ### Namespace & Trademarks {#namespace--trademarks}

              GitLab.com namespaces are available on a first come, first served basis and cannot be reserved. No brand, company, entity, or persons own the rights to any namespace on GitLab.com and may not claim them based on the trademark. Owning the brand "GreatCompany" does not mean owning the namespace "gitlab.com/GreatCompany". Any dispute regarding namespaces and trademarks must be resolved by the parties involved. GitLab Support will never act as arbitrators or intermediaries in these disputes and will not take any action without the appropriate legal orders.

              ### Log requests {#log-requests}

              Due to our [terms](/terms/){data-ga-name="terms" data-ga-location="log requests"}, GitLab Support cannot provide raw copies of logs. However, if users have concerns, Support can answer specific questions and provide summarized information related to the content of log files.

              For paid users on GitLab.com, many actions are logged in the [Audit events](https://docs.gitlab.com/ee/administration/audit_events.html#group-events){data-ga-name="audit events" data-ga-location="body"} section of your GitLab.com project or group.
