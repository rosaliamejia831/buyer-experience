---
  title: GitLab on Red Hat
  description: Scale faster and code better together with a the DevOps platform to build, test, and deploy on Red Hat OpenShift.
  components:
    - name: partners-call-to-action
      data:
        title: GitLab on Red Hat
        inverted: true
        body:
          - header:
          - text: Scale faster and code better together with a the DevOps platform to build, test, and deploy on Red Hat OpenShift. Red Hat and GitLab have invested in our joint partnership to be able to deliver the best possible experience for those customers who choose GitLab and Red Hat for helping to modernize their IT infrastructure. Customers can be assured of having a best in class solution for their DevOps environments as a result of the joint testing and [Red Hat certification for the GitLab Runner](https://catalog.redhat.com/software/operators/search?q=GitLab).
        image:
          image_url: /nuxt-images/partners/redhat/redhat_logo_sqaure.svg
          alt: redhat
    - name: copy-media
      data:
        block:
            - header: From big ideas to deploying on Kubernetes
              text: |
                Iterate faster and innovate together using a single solution for everyone on your pipeline. GitLab is an end-to-end source code management (SCM) and continuous integration (CI) solution for scaling modern applications on Red Hat OpenShift. Leverage GitLab’s tight Kubernetes integration to simplify containerized workload deployments on Red Hat OpenShift Container Platform.
              image:
                image_url: /nuxt-images/partners/redhat/featured-partner-redhat-1.png
              link_href: /blog/2020/04/29/gitlab-and-redhat-automation/
              link_text: Read the blog
              secondary_link_href: /sales/
              secondary_link_text: Talk to an expert
              hide_horizontal_rule: true
    - name: 'copy'
      data:
        block:
          - header: DevOps and Cloud Native App Development
            text: |
              Deploying GitLab and Red Hat OpenShift together supports an organization’s journey to DevOps and cloud native application development, while delivering the powerful CI/CD platform needed to build and run your applications. Software delivery cycle times are collapsed with higher efficiency across all stages of the development lifecycle.

              [Learn more](https://www.openshift.com/try) about Red Hat OpenShift and how to [install the GitLab operator](https://www.openshift.com/blog/installing-the-gitlab-runner-the-openshift-way).
    - name: 'partners-feature-showcase'
      data:
        header: Develop better cloud native applications faster with GitLab and Red Hat OpenShift
        image_url: /nuxt-images/partners/redhat/featured-partner-redhat-2.svg
        text: Shrink cycle times and expand possibilities by driving efficiency at every stage of your software development process with GitLab. Unlock built-in planning, monitoring, and reporting solutions to enable and secure your public, private, or hybrid cloud deployments on Red Hat OpenShift.
        cards:
          - header: Creative contributions
            text: Code what matters. Version control and collaboration reduce rework so happier developers can expand product roadmaps instead of repairing old roads.
          - header: Worry-free workflows
            text: Armor your automation. Increase uptime by reducing security and compliance risks on public, private, and hybrid clouds.
          - header: Reliable results
            text: Succeed, repeat. Increase market share and revenue when your product is on budget, on time, and always up.
    - name: 'benefits'
      data:
        header: Get started with GitLab and Red Hat Joint Solutions
        description: No matter where you’re at in your development journey, GitLab gets it. As an open core platform, GitLab gives you the freedom to maintain the investment in your current toolchain as you modernize. Cloud native developers prefer GitLab’s hybrid cloud CI/CD pipeline and rely on its multicloud strategy with workflow portability to increase operational efficiencies.
        full_background: true
        cards_per_row: 2
        text_align: left
        benefits:
          - icon: /nuxt-images/icons/icon-1.svg
            title: Red Hat Enterprise Linux
            description: Deploy Red Hat Enterprise Linux, the world’s leading enterprise-grade Linux operating system (OS) platform, across the hybrid cloud, from bare metal and virtual servers to private and public cloud environments. Red Hat Enterprise Linux makes it easier for operations teams to manage the upgrades, security patches, and lifecycles of servers that run applications like GitLab.
            link:
              text: Learn more
              url: https://www.redhat.com/en/enterprise-linux-8
          - icon: /nuxt-images/icons/icon-2.svg
            title: Red Hat OpenShift Container Platform (OCP)
            description: Red Hat provides the industry’s most comprehensive enterprise Kubernetes platform in Red Hat OpenShift. OpenShift is uniquely positioned to run containerized applications on public or private clouds. GitLab is a Certified OpenShift Operator, providing Day-1 and Day-2 operations via GitLab Runner.
            link:
              text: Learn more
              url: https://www.openshift.com/try
          - icon: /nuxt-images/icons/icon-3.svg
            title: Red Hat Ansible
            description: 'GitLab enables Infrastructure as Code (IaC) with Ansible. Trigger automation with source control: apply infrastructure configuration changes, deploy new services, reconfigure existing applications, and more. Use GitLab CI to edit and ship code from the Ansible playbook without installing local dependencies.'
            link:
              text: Learn more
              url: https://www.redhat.com/en/technologies/management/ansible
    - name: 'pull-quote'
      data:
        quote: GitLab can accelerate software development and deployment of applications while Red Hat Enterprise Linux can act as the more secure, fully managed OS that can scale with the application. The inclusion of new DevOps tools in Red Hat’s hybrid cloud technologies, like service mesh, empowers developers to iterate faster on a foundation of trusted enterprise Linux.
        source: VICK KELKAR, DIRECTOR OF ALLIANCES AT GITLAB
        link_text: ''
        shadow: true
    - name: 'pull-quote'
      data:
        quote: Red Hat's partner ecosystem is a vital component to delivering innovative, flexible and open solutions. We are pleased to collaborate with GitLab to build, test and certify GitLab Runner in order to help joint customers modernize IT infrastructure and support on-premise and multi-cloud environments.
        source: MIKE WERNER, SENIOR DIRECTOR, GLOBAL ECOSYSTEMS, RED HAT
        link_text: ''
        shadow: true
    - name: copy-resources
      data:
        title: Discover the benefits of GitLab on Red Hat OpenShift
        block:
          - video:
              title: 'Opening Keynote: The Power of GitLab - Sid Sijbrandij'
              video_url: https://www.youtube.com/embed/xn_WP4K9dl8?enablesjsapi=1
              label: Gitlab commit virtual 2020
            resources:
              video:
                header: Video
                links:
                  - text: GitLab Runner on Red Hat Openshift
                    link: https://www.youtube.com/watch?v=5AbtSxpFQec&feature=youtu.be
              blog:
                header: Blogs
                links:
                  - text: GitLab and Red Hat OpenShift- Automation to enhance secure software development
                    link: /blog/2020/04/29/gitlab-and-redhat-automation/
                  - text: Installing the GitLab Runner the OpenShift Way
                    link: https://www.openshift.com/blog/installing-the-gitlab-runner-the-openshift-way
